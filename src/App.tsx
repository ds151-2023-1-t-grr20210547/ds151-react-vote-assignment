import React, { useState } from 'react';
import './App.css';
import Resultado from './Resultado';

import pagodin from './assets/pagodin.jpg';
import vila from './assets/village.jpg';

function App() {
  const [votos, setVotos] = useState({
    'Mr. Pagodin': 0,
    'Village Martin': 0,
  });

  const test = { pagodin }

  const [mostraResultado, setMostraResultado] = useState(false);

  const handleVoto = (pessoa) => {
    setVotos((votosAntes) => ({
      ...votosAntes, // Copia votos passados
      [pessoa]: votosAntes[pessoa] + 1, // Soma 1
    }));
  };

  const finalizarCont = () => {
    setMostraResultado(true);
  };

  return (
    <div className='App'>

      {mostraResultado ? (
        <Resultado candidateVotes={votos} />
      ) : (
        <>
          <h1>Melhor Cantor de JPOP</h1>

          <div style={{marginBottom:'100px'}}>
            <h2>Mr. Pagodin</h2>
            <img src={pagodin} style={{ width: '200px' }} /><br />
            <h3>{votos['Mr. Pagodin']}</h3><br />
            <button onClick={() => handleVoto('Mr. Pagodin')}>Votar</button>
          </div>

          <div style={{marginBottom:'100px'}}>
            <h2>Village Martin</h2>
            <img src={vila} style={{ width: '200px' }} /><br />
            <h3>{votos['Village Martin']}</h3><br />
            <button onClick={() => handleVoto('Village Martin')}>Votar</button>
          </div>

          <button onClick={finalizarCont}>Contabilizar</button>

        </>
      )}
    </div>
  );
}

export default App;
