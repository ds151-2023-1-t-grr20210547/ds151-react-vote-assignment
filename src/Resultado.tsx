import React from 'react';

function ResultPage({ candidateVotes }) {
  const venceu = Object.keys(candidateVotes).reduce(
    (a, b) => (candidateVotes[a] > candidateVotes[b] ? a : b)
  );

  return (
    <div>
      <h1>Resultado da votação</h1>
      <ul>
        {Object.entries(candidateVotes).map(([candidate, votes]) => (
          <li key={candidate}>
            {candidate}: {votes} votos
          </li>
        ))}
      </ul>
      <p>O vencedor é o {venceu} com {candidateVotes[venceu]} votos!</p>
    </div>
  );
}

export default ResultPage;
